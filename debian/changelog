pyme (1:0.8.1-3) UNRELEASED; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Arnaud Fontaine ]
  * Remove Gustavo Franco from Uploaders as he is retiring. Closes: #729399.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/changelog: Remove trailing whitespaces
  * Remove debian/pycompat, it's not used by any modern Python helper
  * Convert git repository from git-dpm to gbp layout
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 17:02:53 -0500

pyme (1:0.8.1-2) unstable; urgency=low

  * debian/control:
    + Bump Standards-Version to 3.9.2. No changes needed.
  * Switch to dpkg-source 3.0 (quilt) format.
    + Update debian/source/format accordingly.
    + No need to remove manually debian/ from the upstream source tarball.
    + Add debian/patches/fix_setup_dash_ftbfs.patch including changes on
      setup.py previously found in diff.gz.
    + debian/control:
      - Remove Build-Depends on quilt.
    + debian/rules:
      - Do not include patchsys-quilt.mk anymore.
  * Use dh_python2 rather than now deprecated dh_pycentral. Closes: #616971.
    + debian/control:
      - Rename now deprecated XS-Python-Version to X-Python-Version.
      - Remove now deprecated XB-Python-Version.
      - Bump cdbs version to 0.4.90-1~.
      - Bump python-all-dev version to 2.6.6-3~.
      - Remove Build-Depends on python-central.
    + debian/rules:
      - Remove DEB_PYTHON_SYSTEM.

 -- Arnaud Fontaine <arnau@debian.org>  Mon, 18 Apr 2011 10:39:58 +0900

pyme (0.8.1+clean-4) unstable; urgency=low

  * Team upload.
  * debian/patches:
    + Add fix_import_error.patch to fix import error with Python >= 2.6
      (closes: #589382). Thanks to Martin Manns for the bug report and to Uldis
      Ansmits for the patch.
  * debian/control:
    + Update Standards-Version to 3.9.1. No changes needed.

 -- Jakub Wilk <jwilk@debian.org>  Mon, 23 Aug 2010 16:27:23 +0200

pyme (0.8.1+clean-3) unstable; urgency=low

  * debian/patches:
    + Add add_missing_check_version.patch to fix breakage in signing.
      Thanks to Marek Hulán. Closes: #550596.

 -- Arnaud Fontaine <arnau@debian.org>  Tue, 27 Oct 2009 17:24:56 +0000

pyme (0.8.1+clean-2) unstable; urgency=low

  [ Arnaud Fontaine ]
  * debian/patches:
    + Add build_with_lfs.patch as required by libgpgme. Closes: #546679.
  * Use quilt to manage patches.
  * debian/copyright:
    + Update copyright years.
  * debian/control:
    + Drop now useless Conflicts and Replaces.
    + Update Standards-Version to 3.8.3.
      - Add README.source.
    + Add ${misc:Depends} to python-pyme and python-pyme-doc Depends in
      case the result of a call to debhelper tools adds extra
      dependencies.

  [ Sandro Tosi ]
  * debian/control:
    + Switch Vcs-Browser field to viewsvn.

 -- Arnaud Fontaine <arnau@debian.org>  Fri, 09 Oct 2009 17:04:22 +0100

pyme (0.8.1+clean-1) unstable; urgency=low

  * New upstream release.
    + Update 01_makefile.patch as a part of the diff has been merged
      upstream.
  * Get rid of debian/ directory in upstream source and add a note about
    that in debian/copyright.

 -- Arnaud Fontaine <arnau@debian.org>  Tue, 02 Dec 2008 16:23:52 +0000

pyme (0.8.0-2) unstable; urgency=low

  * Apply patch which fixes FTBFS with dash. Thanks to Adrien
    Cunin. Closes: #493110.

 -- Arnaud Fontaine <arnau@debian.org>  Fri, 15 Aug 2008 18:51:26 +0200

pyme (0.8.0-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    + Update Standards-Version to 3.8.0.
    + Wrap Uploaders and Build-Depends fields.
  * debian/copyright:
    + Update copyright years.
  * debian/patches:
    + Drop 02_gpgme.patch because it has been applied upstream.
    + Update 01_makefile.patch.

 -- Arnaud Fontaine <arnau@debian.org>  Mon, 30 Jun 2008 18:03:16 +0200

pyme (0.7.0-4) unstable; urgency=low

  [ Arnaud Fontaine ]
  * New email address.
  * debian/control:
    + Update Standards-Version to 3.7.3. No changes needed.

  [ Piotr Ożarowski ]
  * debian/control:
    + Vcs-Svn, Vcs-Browser and Homepage fields added (dpkg support
      them now).

  [ Sandro Tosi ]
  * debian/control
    - Uniforming Vcs-Browser field.

 -- Arnaud Fontaine <arnau@debian.org>  Sat, 23 Feb 2008 11:53:33 +0000

pyme (0.7.0-3) unstable; urgency=low

  * Update Standards-Version to 3.7.2. No changes needed.
  * New Python policy changes. Closes: #373499.
    + Add debian/pycompat.
    + debian/compat:
      - Update debhelper compatibility to 5.
    + debian/rules:
      - Add DEB_PYTHON_SYSTEM=pycentral.
      - Clean debian/rules.
    + debian/control.in:
      - Remove all versioned packages.
      - Add XB-Python-Version field for binary package.
      - Add XS-Python-Version field for source package.
      - Add Conflicts and Replaces fields against previous revisions.

 -- Arnaud Fontaine <arnaud@andesi.org>  Sat, 24 Jun 2006 16:23:45 +0200

pyme (0.7.0-2) unstable; urgency=low

  * Add debian/patches/01_gpgme.patch in order to fix FTBFS issue
    due to swig version 1.3.28. Closes: #358648.
  * Move swig stuff from pre-build rules to post-patches in order to
    patch gpgme.i before executing swig (debian/rules).

 -- Arnaud Fontaine <arnaud@andesi.org>  Wed, 12 Apr 2006 22:21:01 +0200

pyme (0.7.0-1) unstable; urgency=low

  * New upstream release. Thanks to Igor Belyi and John Goerzen for
    maintaining unofficial debian package. Closes: #322168.

 -- Arnaud Fontaine <arnaud@andesi.org>  Thu, 12 Jan 2006 13:57:34 +0100
